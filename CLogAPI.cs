﻿// This class is Auto-Generated
using System;
using System.Diagnostics;
using UnityEngine;

namespace JCMG.CLog
{
    /// <summary>
    /// CLog is a conditional logging static class that allows for logging to be done using domain specific logging
    /// methods that can be compiled in or out of the app via the presence of #defines in LogDefines
    /// </summary>
    public static class CLog
    {

#region App Log API


        [Conditional("LOG_APP")]
        public static void LogAppErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_APP")]
        public static void LogAppError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_APP")]
        public static void LogAppWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_APP")]
        public static void LogAppWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_APP")]
        public static void LogAppInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_APP")]
        public static void LogAppInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_APP")]
        public static void LogAppInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion
#region Game Log API


        [Conditional("LOG_GAME")]
        public static void LogGameErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_GAME")]
        public static void LogGameInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion
#region Input Log API


        [Conditional("LOG_INPUT")]
        public static void LogInputErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_INPUT")]
        public static void LogInputInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion
#region Tutorial Log API


        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_TUTORIAL")]
        public static void LogTutorialInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion
#region UI Log API


        [Conditional("LOG_UI")]
        public static void LogUIErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_UI")]
        public static void LogUIError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_UI")]
        public static void LogUIWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_UI")]
        public static void LogUIWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_UI")]
        public static void LogUIInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_UI")]
        public static void LogUIInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_UI")]
        public static void LogUIInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion
#region Match Log API


        [Conditional("LOG_MATCH")]
        public static void LogMatchErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchErrorFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchError(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogError(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchError(object message)
        {
            UnityEngine.Debug.LogError(message);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchWarningFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchWarningFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchWarning(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchWarning(object message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchInfoFormat(UnityEngine.Object context, string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchInfoFormat(string template, params object[] args)
        {
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchInfo(UnityEngine.Object context, object message)
        {
            UnityEngine.Debug.Log(message, context);
        }

        [Conditional("LOG_MATCH")]
        public static void LogMatchInfo(object message)
        {
            UnityEngine.Debug.Log(message);
        }

#endregion

    }
}
