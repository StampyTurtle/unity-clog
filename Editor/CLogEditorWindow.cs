﻿/*
CLog 
Copyright (c) 2018 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
using System;
using System.Collections.Generic;
using System.Text;
using UnityEditor;

namespace JCMG.CLog.Editor
{
    public class CLogEditorWindow : EditorWindow
    {
        private class LogToggleData
        {
            public bool Enabled;
            public CLogDefineType CLogDefineType;
            public string Symbol;
        }

        private CLogDefineType[] _allCLogDefineType;
        private List<string> _symbolDefines = new List<string>();
        private List<LogToggleData> _logToggleDatas = new List<LogToggleData>();

        private StringBuilder _sb = new StringBuilder();
        private const string _symbolFormat = "LOG_{0}";
        
        [MenuItem("JCMG/CLog/Open CLog Window")]
        [MenuItem("Window/CLog")]
        public static void OpenLoggingWindow()
        {
            var window = GetWindow<CLogEditorWindow>();
            window.Show();
        }

        private void OnEnable()
        {
            titleContent.text = "Logging";
            
            // Grab all symbols define in the build group currently 
            var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(GetCurrentBuildGroup());
            _symbolDefines.AddRange(ParseSymbols(symbols));
            
            // Create data bindings for all LogDefineType values so that a user
            // can toggle them off or on.
            _allCLogDefineType = (CLogDefineType[])Enum.GetValues(typeof(CLogDefineType));
            for (var i = 0; i < _allCLogDefineType.Length; i++)
            {
                var logToggleData = new LogToggleData()
                {
                    CLogDefineType = _allCLogDefineType[i]
                };
                
                var symbolName = string.Format(_symbolFormat, _allCLogDefineType[i].ToString().ToUpper());

                logToggleData.Symbol = symbolName;
                logToggleData.Enabled = _symbolDefines.Contains(symbolName);
                
                _logToggleDatas.Add(logToggleData);
            }
        }

        private void OnDisable()
        {
            _symbolDefines.Clear();
            _logToggleDatas.Clear();
            _sb.Remove(0, _sb.Length);
        }

        private string[] ParseSymbols(string source)
        {
            return source.Split(';');
        }

        private static BuildTargetGroup GetCurrentBuildGroup()
        {
            return EditorUserBuildSettings.selectedBuildTargetGroup;
        }

        private void OnGUI()
        {
            // Show a vertical list of toggles to be able to turn on or off a particular LogDefineType
            EditorGUILayout.TextField("Log Defines", EditorStyles.boldLabel);
            
            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.BeginVertical();

            for (var i = 0; i < _logToggleDatas.Count; i++)
            {
                var ltd = _logToggleDatas[i];
                ltd.Enabled = EditorGUILayout.Toggle(ltd.CLogDefineType.ToString(), ltd.Enabled);                
            }
            
            EditorGUILayout.EndVertical();
            
            // If we have toggle any log define type on or off, get the combined string of all log defined types 
            // in the syntax that matches the conditional log attributes.
            if (EditorGUI.EndChangeCheck ()) 
            {
                // 1. Get current scripting symbols 
                // 2. Remove any specific to the logging
                // 3. Add to string builder any for logging that are enabled and any unrelated scripting defines.
                var currentScriptDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(GetCurrentBuildGroup());
                var currentScriptDefineEntries = new List<string>(ParseSymbols(currentScriptDefines));

                _sb.Remove(0, _sb.Length);
                for (var i = 0; i < _logToggleDatas.Count; i++)
                {
                    var ltd = _logToggleDatas[i];

                    currentScriptDefineEntries.Remove(ltd.Symbol);

                    if (!ltd.Enabled) continue;
                    
                    _sb.Append(string.Format("{0};", ltd.Symbol));
                }

                for (var i = 0; i < currentScriptDefineEntries.Count; i++)
                    _sb.Append(currentScriptDefineEntries[i]);
                
                PlayerSettings.SetScriptingDefineSymbolsForGroup(GetCurrentBuildGroup(), _sb.ToString());
            }
        }
    }
}