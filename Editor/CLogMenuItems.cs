﻿/*
CLog 
Copyright (c) 2018 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
using System;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace JCMG.CLog.Editor
{
    public static class CLogMenuItems
    {
        [MenuItem("JCMG/CLog/Refresh Clog API", priority = 0)]
        public static void RefreshReadmeMenuItems()
        {
            // the generated filepath. This will need to change if CLog is put in a different folder.
            var scriptFilePath = Application.dataPath + "/JCMG/Clog/CLogAPI.cs";

            // The class string
            var sb = new StringBuilder();
            sb.AppendLine("// This class is Auto-Generated");
            sb.AppendLine("using System;");
            sb.AppendLine("using System.Diagnostics;");
            sb.AppendLine("using UnityEngine;");
            sb.AppendLine("");
            sb.AppendLine(@"namespace JCMG.CLog
{
    /// <summary>
    /// CLog is a conditional logging static class that allows for logging to be done using domain specific logging
    /// methods that can be compiled in or out of the app via the presence of #defines in LogDefines
    /// </summary>
    public static class CLog
    {");
            sb.AppendLine("");

            var allClogTypes = ((CLogDefineType[])Enum.GetValues(typeof(CLogDefineType))).ToList();
            allClogTypes.Sort();

            foreach (var clogType in allClogTypes)
            {
                sb.AppendLine(string.Format("#region {0} Log API", clogType));
                sb.Append(string.Format(API_DEFINITION, clogType.ToString().ToUpper(), clogType));
                sb.AppendLine("#endregion");
            }

            sb.AppendLine("");
            sb.AppendLine(@"    }");
            sb.AppendLine("}");

            // writes the class and imports it so it is visible in the Project window
            System.IO.File.Delete(scriptFilePath);
            System.IO.File.WriteAllText(scriptFilePath, sb.ToString(), Encoding.UTF8);
            AssetDatabase.ImportAsset("Assets/JCMG/Clog/CLogAPI.cs");
        }

        private static string API_DEFINITION = @"

        [Conditional(""LOG_{0}"")]
        public static void Log{1}ErrorFormat(UnityEngine.Object context, string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}ErrorFormat(string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.LogError(message);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Error(UnityEngine.Object context, object message)
        {{
            UnityEngine.Debug.LogError(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Error(object message)
        {{
            UnityEngine.Debug.LogError(message);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}WarningFormat(UnityEngine.Object context, string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}WarningFormat(string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.LogWarning(message);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Warning(UnityEngine.Object context, object message)
        {{
            UnityEngine.Debug.LogWarning(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Warning(object message)
        {{
            UnityEngine.Debug.LogWarning(message);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}InfoFormat(UnityEngine.Object context, string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}InfoFormat(string template, params object[] args)
        {{
            var message = string.Format(template, args);
            UnityEngine.Debug.Log(message);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Info(UnityEngine.Object context, object message)
        {{
            UnityEngine.Debug.Log(message, context);
        }}

        [Conditional(""LOG_{0}"")]
        public static void Log{1}Info(object message)
        {{
            UnityEngine.Debug.Log(message);
        }}

";
    }
}
